package com.germancuesta.motionlayoutdemo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.germancuesta.motionlayoutdemo.layoutmanager.RecyclerMain2Activity
import com.germancuesta.motionlayoutdemo.layoutmanager.RecyclerMainActivity
import com.germancuesta.motionlayoutdemo.presentation.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        scene1_part1.setOnClickListener {
            startActivity(Intent(this, Scene1Part1Activity::class.java))
        }
        scene1_part2.setOnClickListener {
            startActivity(Intent(this, Scene1Part2Activity::class.java))
        }
        scene1_part3.setOnClickListener {
            startActivity(Intent(this, Scene1Part3Activity::class.java))
        }
        scene2_part1.setOnClickListener {
            startActivity(Intent(this, Scene2Part1Activity::class.java))
        }
        scene2_part2.setOnClickListener {
            startActivity(Intent(this, Scene2Part2Activity::class.java))
        }
        scene2_part3.setOnClickListener {
            startActivity(Intent(this, Scene2Part3Activity::class.java))
        }
        scene2_part4.setOnClickListener {
            startActivity(Intent(this, Scene2Part4Activity::class.java))
        }
        button_combinate.setOnClickListener {
            startActivity(Intent(this, CombinationActivity::class.java))
        }
        layout_manager_1.setOnClickListener {
            startActivity(Intent(this, RecyclerMainActivity::class.java))
        }
        layout_manager_2.setOnClickListener {
            startActivity(Intent(this, RecyclerMain2Activity::class.java))
        }
    }
}