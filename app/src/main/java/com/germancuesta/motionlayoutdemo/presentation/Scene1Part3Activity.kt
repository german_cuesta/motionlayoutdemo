package com.germancuesta.motionlayoutdemo.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.germancuesta.motionlayoutdemo.R

class Scene1Part3Activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scene1_part3)
    }
}