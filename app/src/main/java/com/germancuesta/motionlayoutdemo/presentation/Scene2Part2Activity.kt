package com.germancuesta.motionlayoutdemo.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.germancuesta.motionlayoutdemo.R

class Scene2Part2Activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scene2_part2)
    }
}