package com.germancuesta.motionlayoutdemo.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.germancuesta.motionlayoutdemo.R

class CombinationActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_combination)
    }
}