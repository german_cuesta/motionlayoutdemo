package com.germancuesta.motionlayoutdemo.layoutmanager

import android.content.res.Resources
import android.util.Log
import android.view.View
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import androidx.core.math.MathUtils
import androidx.recyclerview.widget.RecyclerView
import com.germancuesta.motionlayoutdemo.R

class CustomLayoutManager(resources: Resources, private val screenWidth: Int): RecyclerView.LayoutManager() {
    private var horizontalScrollOffset = 0

    private val viewWidth = resources.getDimensionPixelSize(R.dimen.item_width)
    private val recyclerViewHeight = (resources.getDimensionPixelSize(R.dimen.recyclerview_height)).toDouble()

    override fun generateDefaultLayoutParams() =
        RecyclerView.LayoutParams(WRAP_CONTENT, WRAP_CONTENT)

    override fun canScrollHorizontally() = true

    override fun scrollHorizontallyBy(
        dx: Int,
        recycler: RecyclerView.Recycler,
        state: RecyclerView.State?
    ): Int {
        horizontalScrollOffset += dx
        fill(recycler)
        return dx
    }

    override fun onLayoutChildren(recycler: RecyclerView.Recycler, state: RecyclerView.State?) {
        fill(recycler)
    }

    private fun fill(recycler: RecyclerView.Recycler) {
        detachAndScrapAttachedViews(recycler)

        var firstVisiblePosition = Math.floor(horizontalScrollOffset.toDouble() / viewWidth.toDouble()).toInt()
        val lastVisiblePosition = (horizontalScrollOffset + screenWidth) / viewWidth

        Log.e("CustomLayoutManager", "firstVisiblePosition $firstVisiblePosition")
        Log.e("CustomLayoutManager", "lastVisiblePosition $lastVisiblePosition")
        Log.e("CustomLayoutManager", "horizontalScrollOffset $horizontalScrollOffset")

        for (index in firstVisiblePosition..lastVisiblePosition) {
            var recyclerIndex = index % itemCount
            if (recyclerIndex < 0) {
                recyclerIndex += itemCount
            }
            val view = recycler.getViewForPosition(recyclerIndex)
            addView(view)

//            layoutChildView(index, viewWidth, view)
            layoutChildViewNew(index, viewWidth, view)
        }

        val scrapListCopy = recycler.scrapList.toList()
        scrapListCopy.forEach { recycler.recycleView(it.itemView) }
    }

    private fun layoutChildViewNew(i: Int, viewWidthWithSpacing: Int, view: View) {
        val left = i * viewWidthWithSpacing - horizontalScrollOffset//if (horizontalScrollOffset == 0) {
//            i * viewWidthWithSpacing - horizontalScrollOffset + (screenWidth / 4)
//        } else {
//            i * viewWidthWithSpacing - horizontalScrollOffset + ((screenWidth - view.layoutParams.width) / 2)
//        }
        val right = left + viewWidth
        val top = 0 //getRotationAngleOffsetForView(left + viewWidth/2)
        val bottom = view.layoutParams.height

        view.rotationY = getRotationAngleOffsetForView(left + (viewWidth / 2))
        measureChild(view, viewWidth, viewWidth)
        layoutDecorated(view, left, top, right, bottom)
    }

    private fun getRotationAngleOffsetForView(viewCentreX: Int): Float {
        val dx = (screenWidth / 2) - viewCentreX
        return (dx * 30f) / (screenWidth / 2)
    }

//    private fun layoutChildView(i: Int, viewWidthWithSpacing: Int, view: View) {
//        val left = i * viewWidthWithSpacing - horizontalScrollOffset
//        val right = left + viewWidth
//        val top = getTopOffsetForView(left + viewWidth/2)
//        val bottom = top + viewWidth
//
//        measureChildWithMargins(view, viewWidth, viewWidth)
//
//        layoutDecoratedWithMargins(view, left, top, right, bottom)
//    }
//
//    private fun getTopOffsetForView(viewCentreX: Int): Int {
//        val s: Double = screenWidth.toDouble() / 2
//        val h: Double = recyclerViewHeight - viewWidth.toDouble()
//        val radius: Double = ( h*h + s*s ) / (h*2)
//
//        val cosAlpha = (s - viewCentreX) / radius
//        val alpha = Math.acos(MathUtils.clamp(cosAlpha, -1.0, 1.0))
//
//        val yComponent = radius - (radius * Math.sin(alpha))
//        return yComponent.toInt()
//    }

}