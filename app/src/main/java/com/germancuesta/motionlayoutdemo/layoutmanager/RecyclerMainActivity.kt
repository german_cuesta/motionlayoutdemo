package com.germancuesta.motionlayoutdemo.layoutmanager

import android.graphics.Point
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SnapHelper
import com.germancuesta.motionlayoutdemo.R
import com.germancuesta.motionlayoutdemo.databinding.CardItemViewBinding
import kotlinx.android.synthetic.main.activity_recycler_main.*

class RecyclerMainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recycler_main)
        initView()
    }

    private fun initView() {
        val size = Point()
        windowManager.defaultDisplay.getSize(size)
        val screenWidth = size.x

        recyclerView_id.layoutParams.height = screenWidth / 2
        val helper: SnapHelper = PagerSnapHelper()
        helper.attachToRecyclerView(recyclerView_id)
        val list = listOf<String>("string 1", "string 2", "string 3", "string 4",
            "string 5", "string 6", "string 7", "string 8", "string 9", "string 10", "string 11")
        val adapterr = Adapter().apply { setList(list) }
        recyclerView_id.apply {
            adapter = adapterr
            layoutManager = CustomLayoutManager2(resources, screenWidth)
        }
    }

    inner class Adapter: RecyclerView.Adapter<Adapter.ViewHolder>() {
        private val listData = mutableListOf<String>()

        fun setList(list: List<String>) {
            listData.addAll(list)
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Adapter.ViewHolder {
            val binding =
                CardItemViewBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false)

            return ViewHolder(binding)
        }

        override fun getItemCount() = listData.size

        override fun onBindViewHolder(holder: Adapter.ViewHolder, position: Int) {
            holder.bind(listData[position])
        }

        inner class ViewHolder(private val bindingItem: CardItemViewBinding)
            : RecyclerView.ViewHolder(bindingItem.root) {

            fun bind(value: String) {
                bindingItem.cardView.setOnClickListener {
                    if (motion.currentState == motion.endState) {
                        motion.transitionToStart()
                        return@setOnClickListener
                    }

                    motion.transitionToEnd()
                }
            }
        }
    }
}